package org.jsoll.xml;

import java.util.Comparator;

public class IPDocumentComparator implements Comparator<IPDocumentXml> {
	private boolean compararNumero;
	private String propertyName;
	private SorterOrder order;

	public IPDocumentComparator(String propertyName) {
		this(propertyName, SorterOrder.ASC, false);
	}

	public IPDocumentComparator(String propertyName, SorterOrder order) {
		this(propertyName, order, false);
	}

	public IPDocumentComparator(String propertyName, SorterOrder order, boolean numerico) {
		this.propertyName = propertyName;
		this.compararNumero = numerico;
		this.order = order;
	}

	private int compareInt(int int1, int int2) {
		return Integer.compare(int1, int2);
	}

	private int getStringAsInt(String s) {
		return Integer.parseInt(s);
	}

	@Override
	public int compare(IPDocumentXml o1, IPDocumentXml o2) {
		if (this.compararNumero) {
			return (this.order == SorterOrder.ASC) ? compareInt(getStringAsInt(o1.getAttrValue(propertyName)), getStringAsInt(o2.getAttrValue(propertyName))) :
				compareInt(getStringAsInt(o2.getAttrValue(propertyName)), getStringAsInt(o1.getAttrValue(propertyName)));
		} else {
			String s1 = o1.getAttrValue(propertyName) + "";
			String s2 = o2.getAttrValue(propertyName) + "";
			return (this.order == SorterOrder.ASC) ? s1.compareToIgnoreCase(s2) : s2.compareToIgnoreCase(s1);
		}
	}

}
