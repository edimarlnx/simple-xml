package org.jsoll.xml;

public enum SorterOrder {
	ASC,
	DESC
}
