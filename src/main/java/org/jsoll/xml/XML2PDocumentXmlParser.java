package org.jsoll.xml;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class XML2PDocumentXmlParser extends DefaultHandler {
	private IPDocumentXml xmlDoc;
	private IPDocumentXml childNode;
	private String encoding = "UTF-8";

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (xmlDoc.getTagName().equals("SAXStart")) {
			childNode = xmlDoc;
			childNode.setEncoding(this.encoding);
		} else {
			childNode = childNode.addChild(qName);
		}
		childNode.setTagName(qName);
		for (int i = 0; i < attributes.getLength(); i++) {
			childNode.addOrUpdateAttr(attributes.getQName(i), attributes.getValue(i));
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (!String.copyValueOf(ch, start, length).trim().isEmpty())
			if (childNode.getContent() == null)
				childNode.setContent(String.copyValueOf(ch, start, length));
			else
				childNode.setContent(childNode.getContent() + String.copyValueOf(ch, start, length));
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (childNode.getParent() != null)
			childNode = childNode.getParent();
	}

	@Override
	public void startDocument() throws SAXException {
		this.xmlDoc = new PDocumentXml("SAXStart");
	}

	public IPDocumentXml loadXml(InputStream xmlData) {
		return loadXml(xmlData, "UTF-8");
	}

	public IPDocumentXml loadXml(InputStream xmlData, String encoding) {
		if (encoding.equals(""))
			encoding = "UTF-8";
		this.encoding = encoding;
		try {
			SAXParser saxp = SAXParserFactory.newInstance().newSAXParser();
			Reader reader = new InputStreamReader(xmlData);
			InputSource inputSource = new InputSource();
			inputSource.setCharacterStream(reader);
			inputSource.setEncoding(this.encoding);
			saxp.parse(inputSource, this);
			return xmlDoc;
		} catch (Exception ex) {
			Logger.getLogger(XML2PDocumentXmlParser.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

}
