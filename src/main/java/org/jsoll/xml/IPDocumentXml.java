package org.jsoll.xml;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface IPDocumentXml {

	//Childs
	List<IPDocumentXml> getChilds();

	IPDocumentXml addChild(String tagName, String content);

	IPDocumentXml addChild(String tagName);

	IPDocumentXml addChild(IPDocumentXml child);

	boolean removeChild(int index);

	void removeFromTree();

	int getIndex();

	//Attributes
	Map<String, String> getAttributes();

	IPDocumentXml addOrUpdateAttr(String name, String value);

	String getAttrValue(String name);

	IPDocumentXml removeAttr(String name);

	// Content
	String getContent();

	void setContent(String content);

	// Others
	IPDocumentXml getParent();

	void setParent(IPDocumentXml parent);

	IPDocumentXml getDocumentRoot();

	String getTagName();

	void setTagName(String tagName);

	String getEncoding();

	void setEncoding(String encode);

	void sortByAttrValue(String attrName);

	void sortByAttrValue(String attrName, SorterOrder order);

	void sortByAttrValueInt(String attrName);

	void sortByAttrValueInt(String attrName, SorterOrder ordem);

	// Output
	String getXml();

	void SaveXmlFile(File xmlFile);

	//Loads
	void loadXml(String xmlData);

	void loadXmlFile(File xmlData);

}
