package org.jsoll.xml;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PDocumentXmlParser {

	private final IPDocumentXml XMLDoc;
	private final String indent;

	private OutputStream outStream;

	public PDocumentXmlParser(IPDocumentXml XMLDoc) {
		this.XMLDoc = XMLDoc;
		this.indent = "\t";
	}

	private String charIndentFromNivel(int nivel) {
		String c = "";
		for (int i = 0; i < nivel; i++) {
			c += this.indent;
		}
		return c;
	}

	private void writeOutput(String s) {
		try {
			outStream.write(s.getBytes());
		} catch (IOException ex) {
			Logger.getLogger(PDocumentXmlParser.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void closeOuput() {
		try {
			outStream.close();
		} catch (IOException ex) {
			Logger.getLogger(PDocumentXmlParser.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void parseAttributes(IPDocumentXml XMLDocNode) {
		Map<String, String> attrs = XMLDocNode.getAttributes();
		for (String key : attrs.keySet()) {
			writeOutput(" " + key + "=\"" + attrs.get(key) + "\"");
		}
	}

	private void parseChilds(IPDocumentXml XMLDocNode, int nivel) {
		if (XMLDocNode.getParent() == null) {
			writeOutput("<?xml version=\"1.0\" encoding=\"" + XMLDocNode.getEncoding() + "\" ?>");
		}
		writeOutput("\n" + charIndentFromNivel(nivel) + "<" + XMLDocNode.getTagName());
		parseAttributes(XMLDocNode);
		List<IPDocumentXml> childs = XMLDocNode.getChilds();
		if (childs.size() == 0 && XMLDocNode.getContent() == null) {
			writeOutput("/>");
		} else {
			writeOutput(">");
			if (XMLDocNode.getContent() != null && childs.size() == 0) {
				writeOutput(XMLDocNode.getContent());
				writeOutput("</" + XMLDocNode.getTagName() + ">");
			} else {
				if (XMLDocNode.getContent() != null) {
					new Exception("Elemento com conteúdo e elementos filhos não são permitidos. Conteúdo do campo ignorado.").printStackTrace();
				}
				//writeOutput("\n");
				for (IPDocumentXml child : childs) {
					parseChilds(child, nivel + 1);
				}
				writeOutput("\n" + charIndentFromNivel(nivel) + "</" + XMLDocNode.getTagName() + ">");
			}
		}
	}

	public String getXml() {
		ByteArrayOutputStream strWtr = new ByteArrayOutputStream();
		outStream = new PrintStream(strWtr, true);
		parseChilds(XMLDoc, 0);
		closeOuput();
		return strWtr.toString();
	}

	public void saveXmlFile(File outFile) {
		try {
			outStream = new FileOutputStream(outFile);
			parseChilds(XMLDoc, 0);
			closeOuput();
			//return parseChilds(XMLDoc, 0);

		} catch (FileNotFoundException ex) {
			Logger.getLogger(PDocumentXmlParser.class
				.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
