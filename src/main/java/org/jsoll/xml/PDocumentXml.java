package org.jsoll.xml;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PDocumentXml implements IPDocumentXml {
	protected List<IPDocumentXml> childs;
	protected Map<String, String> attributes;
	protected String content;
	protected String tagName;
	protected String encoding;
	protected IPDocumentXml parent;

	public PDocumentXml() {
		this("root");
	}

	public PDocumentXml(String tagName) {
		this.tagName = tagName;
		this.childs = new LinkedList<>();
		this.attributes = new LinkedHashMap<>(0);
		this.encoding = "UTF-8";
		this.content = null;
	}

	//Childs
	@Override
	public List<IPDocumentXml> getChilds() {
		return this.childs;
	}

	@Override
	public IPDocumentXml addChild(String tagName, String content) {
		IPDocumentXml child = this.addChild(new PDocumentXml(tagName));
		child.setContent(content);
		return child;
	}

	@Override
	public IPDocumentXml addChild(String tagName) {
		return addChild(tagName, null);
	}

	@Override
	public IPDocumentXml addChild(IPDocumentXml child) {
		if (child.getParent() != null) {
			child.getParent().getChilds().remove(child);

		}
		child.setParent(this);
		this.childs.add(child);
		return child;
	}

	@Override
	public boolean removeChild(int index) {
		if (index < this.childs.size()) {
			this.childs.remove(index);
			return true;
		} else {
			Logger.getLogger(PDocumentXml.class.getName()).log(Level.WARNING, "Indíce fora da faixa da lista.");
			return false;
		}
	}

	@Override
	public void removeFromTree() {
		if (this.parent != null) {
			this.parent.getChilds().remove(this);
			this.parent = null;
		}
		try {
			this.finalize();
		} catch (Throwable ex) {
			Logger.getLogger(PDocumentXml.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public int getIndex() {
		return (this.parent != null) ? this.parent.getChilds().indexOf(this) : 0;
	}

	//Attributes
	@Override
	public Map<String, String> getAttributes() {
		return this.attributes;
	}

	@Override
	public IPDocumentXml addOrUpdateAttr(String name, String value) {
		this.attributes.put(name, value);
		return this;
	}

	public String getAttrValue(String name) {
		if (this.attributes.containsKey(name))
			return this.attributes.get(name);
		else {
			Logger.getLogger(PDocumentXml.class.getName()).log(Level.WARNING, "Atributo não encontrado na lista.");
			return null;
		}
	}

	@Override
	public IPDocumentXml removeAttr(String name) {
		if (this.attributes.containsKey(name))
			this.attributes.remove(name);
		return this;
	}

	// Others
	@Override
	public String getContent() {
		return this.content;
	}

	@Override
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public IPDocumentXml getParent() {
		return this.parent;
	}

	@Override
	public void setParent(IPDocumentXml parent) {
		this.parent = parent;
	}

	@Override
	public IPDocumentXml getDocumentRoot() {
		IPDocumentXml root = this;
		while (root.getParent() != null)
			root = root.getParent();
		return root;
	}

	@Override
	public String getTagName() {
		return this.tagName;
	}

	@Override
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	@Override
	public String getEncoding() {
		return this.encoding;
	}

	@Override
	public void setEncoding(String encode) {
		this.encoding = encode;
	}

	@Override
	public void sortByAttrValue(String attrName) {
		sortByAttrValue(attrName, SorterOrder.ASC);
	}

	@Override
	public void sortByAttrValue(String attrName, SorterOrder ordem) {
		Collections.sort(childs, new IPDocumentComparator(attrName, ordem));
	}

	@Override
	public void sortByAttrValueInt(String attrName) {
		sortByAttrValueInt(attrName, SorterOrder.ASC);
	}

	@Override
	public void sortByAttrValueInt(String attrName, SorterOrder ordem) {
		Collections.sort(childs, new IPDocumentComparator(attrName, ordem, true));
	}


	// Output
	@Override
	public String getXml() {
		PDocumentXmlParser parse = new PDocumentXmlParser(this);
		return parse.getXml();
	}

	@Override
	public void SaveXmlFile(File xmlFile) {
		new PDocumentXmlParser(this).saveXmlFile(xmlFile);
	}

	// Loads

	protected void loadFromStream(InputStream in) {
		XML2PDocumentXmlParser xmlParser = new XML2PDocumentXmlParser();
		IPDocumentXml tmpXmlDoc = xmlParser.loadXml(in, this.getDocumentRoot().getEncoding());
		this.attributes = tmpXmlDoc.getAttributes();
		this.childs = tmpXmlDoc.getChilds();
		this.content = tmpXmlDoc.getContent();
		this.encoding = tmpXmlDoc.getEncoding();
		this.tagName = tmpXmlDoc.getTagName();
	}

	@Override
	public void loadXml(String xmlData) {
		loadFromStream(new ByteArrayInputStream(xmlData.getBytes()));
	}

	@Override
	public void loadXmlFile(File xmlData) {
		try {
			loadFromStream(new FileInputStream(xmlData));
		} catch (FileNotFoundException ex) {
			Logger.getLogger(PDocumentXml.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
