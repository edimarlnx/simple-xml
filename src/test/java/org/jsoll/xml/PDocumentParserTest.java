package org.jsoll.xml;

import org.testng.Assert;
import org.testng.annotations.Test;


public class PDocumentParserTest {

	@Test
	public void testGetXml() {
		IPDocumentXml xmlDoc = new PDocumentXml();
		System.out.println("getXml - XML em branco.");
		PDocumentXmlParser instance = new PDocumentXmlParser(xmlDoc);
		String expResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<root/>";
		String result = instance.getXml();
		Assert.assertEquals(expResult, result);

		System.out.println("getXml - XML em branco Alterado Tag.");
		xmlDoc.setTagName("job");
		instance = new PDocumentXmlParser(xmlDoc);
		expResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<job/>";
		result = instance.getXml();
		Assert.assertEquals(expResult, result);

		System.out.println("getXml - XML com propriedades sem childs.");
		xmlDoc.setTagName("job");
		xmlDoc.addOrUpdateAttr("SERVICO", "01");
		xmlDoc.addOrUpdateAttr("CLIENTE", "PMX");
		instance = new PDocumentXmlParser(xmlDoc);
		expResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<job SERVICO=\"01\" CLIENTE=\"PMX\"/>";
		result = instance.getXml();
		Assert.assertEquals(expResult, result);

		System.out.println("getXml - XML com propriedades e childs.");
		xmlDoc.setTagName("job");
		xmlDoc.addOrUpdateAttr("SERVICO", "01");
		xmlDoc.addOrUpdateAttr("CLIENTE", "PMX");

		IPDocumentXml child = xmlDoc.addChild("config").addOrUpdateAttr("nome", "D");
		Assert.assertEquals(child.addChild("config2").addOrUpdateAttr("idade", "2").getIndex(), 0);
		Assert.assertEquals(child.addChild("config2").addOrUpdateAttr("idade", "5").getIndex(), 1);
		Assert.assertEquals(child.addChild("config2").addOrUpdateAttr("idade", "1").getIndex(), 2);

		child = xmlDoc.addChild("config").addOrUpdateAttr("nome", "A");
		child.addChild("config2").addOrUpdateAttr("idade", "2");
		child.addChild("config2").addOrUpdateAttr("idade", "5");
		child.addChild("config2").addOrUpdateAttr("idade", "1");
		child.addChild("config2").addOrUpdateAttr("idade", "15");
		child.addChild("config2").addOrUpdateAttr("idade", "16");
		child.addChild("config2").addOrUpdateAttr("idade", "21");

		child = xmlDoc.addChild("config").addOrUpdateAttr("nome", "G");
		child.addChild("config2").addOrUpdateAttr("idade", "2");
		child.addChild("config2").addOrUpdateAttr("idade", "5");
		child.addChild("config2").addOrUpdateAttr("idade", "1");

		//System.out.println("UnSorted>>>>>\n"+xmlDoc.getXml());

		xmlDoc.sortByAttrValue("nome");

		//System.out.println("Sorted>>>>>\n"+xmlDoc.getXml());

		xmlDoc.getChilds().get(0).sortByAttrValue("idade", SorterOrder.ASC);
		//System.out.println("SortedNum>>>>>\n"+xmlDoc.getXml());
		Assert.assertEquals(xmlDoc.getChilds().get(0).getChilds().get(1).getIndex(), 1);
		Assert.assertEquals(xmlDoc.getChilds().get(0).getChilds().get(2).getIndex(), 2);
		Assert.assertEquals(xmlDoc.getChilds().get(0).getChilds().get(0).getIndex(), 0);

		instance = new PDocumentXmlParser(xmlDoc);
		expResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
			"<job SERVICO=\"01\" CLIENTE=\"PMX\">\n" +
			"	<config nome=\"A\">\n" +
			"		<config2 idade=\"1\"/>\n" +
			"		<config2 idade=\"15\"/>\n" +
			"		<config2 idade=\"16\"/>\n" +
			"		<config2 idade=\"2\"/>\n" +
			"		<config2 idade=\"21\"/>\n" +
			"		<config2 idade=\"5\"/>\n" +
			"	</config>\n" +
			"	<config nome=\"D\">\n" +
			"		<config2 idade=\"2\"/>\n" +
			"		<config2 idade=\"5\"/>\n" +
			"		<config2 idade=\"1\"/>\n" +
			"	</config>\n" +
			"	<config nome=\"G\">\n" +
			"		<config2 idade=\"2\"/>\n" +
			"		<config2 idade=\"5\"/>\n" +
			"		<config2 idade=\"1\"/>\n" +
			"	</config>\n" +
			"</job>";
		result = instance.getXml();
		instance.saveXmlFile(ResourceUtils.getResource("/xml_teste/", "xml_teste_2.xml"));
		System.out.println(result);
		Assert.assertEquals(expResult, result);
		Assert.assertEquals(expResult, xmlDoc.getXml());
	}


}
