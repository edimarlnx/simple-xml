package org.jsoll.xml;

import java.io.File;

public class ResourceUtils {
	public static File getResource(String path, String name) {
		String resource = ResourceUtils.class.getResource(path).getFile();
		return new File(resource + name);
	}
}
